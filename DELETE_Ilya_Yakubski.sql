DELETE FROM inventory
WHERE film_id = (SELECT film_id FROM film WHERE title = 'THE TERMINATOR');

DELETE FROM rental
WHERE inventory_id IN (SELECT inventory_id FROM inventory WHERE film_id IS NULL);

DO $$ 
DECLARE 
    your_customer_id INT;
BEGIN
    SELECT customer_id INTO your_customer_id 
    FROM customer 
    WHERE first_name = 'ILYA' AND last_name = 'YAKUBSKI';

    DELETE FROM payment WHERE rental_id IN (SELECT rental_id FROM rental WHERE customer_id = your_customer_id);
    DELETE FROM rental WHERE customer_id = your_customer_id;
END $$;
